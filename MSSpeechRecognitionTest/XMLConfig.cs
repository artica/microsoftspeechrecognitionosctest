﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


namespace MSSpeechRecognitionTest
{

    public class Choice
    {
        [XmlAttribute("Detect")]
        public string Detect { get; set; }
        [XmlAttribute("Response")]
        public string Response { get; set; }
    }

   /* public class ChoicesXML
    {
        [XmlElement("Choice")]
        public List<Choice> Choices { get; set; }
    }*/

    [XmlType("XMLConfig")]
    public class XMLConfig
    {
        [XmlAttribute]
        public string CultureInfo = "pt-PT";
        [XmlAttribute]
        public string ReadyMessage = "hello world";
        [XmlAttribute]
        public int Port = 7400;
        [XmlAttribute]
        public string MessageAddress = "/";
        [XmlAttribute]
        public string IPAddress = "127.0.0.1";
        [XmlElement("Choice")]
        public List<Choice> Choices { get; set; }

        public static XMLConfig LoadFromXML(string path)
        {
            XMLConfig conf;
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var reader = XmlReader.Create(stream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(XMLConfig));
                    conf = (XMLConfig)xmlSerializer.Deserialize(reader);
                }
            }
            return conf;
        }

        public void SaveXml(string path)
        {
            using (FileStream stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    var xmlSerializer = new XmlSerializer(this.GetType());
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");

                    xmlSerializer.Serialize(writer, this, ns);
                    //var xmlEncodedList = Encoding.UTF8.GetString(stream.ToArray());
                }
            }
        }
    }
}
