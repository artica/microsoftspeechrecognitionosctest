
followed this code example
http://stackoverflow.com/questions/3600390/microsoft-speech-recognition-platform

requires to download and install:

1. Microsoft Speech Platform - Server Runtime (both x64 and x86)
https://www.microsoft.com/en-us/download/details.aspx?id=24974

2. Microsoft Speech Platform - SDK
https://www.microsoft.com/en-us/download/details.aspx?id=24003

3. Microsoft Speech Platform - Server Runtime Languages
https://www.microsoft.com/en-us/download/details.aspx?id=3971

need to add reference to load files of SDK

uses 3rd party SharpOSC
https://github.com/ValdemarOrn/SharpOSC


To run the binary afterwards you need to install
MSSpeech_SR_pt-PT_TELE.msi
and
SpeechPlatformRuntime.msi
