﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Net;

using Microsoft.Speech.Recognition;

namespace MSSpeechRecognitionTest
{
    public partial class SpeechRecognizerTest : Form
    {
        private XMLConfig conf;

        public SpeechRecognizerTest()
        {
            //TODO: if xml doesnt exist, create new and save it
            conf = XMLConfig.LoadFromXML("XMLConfig.xml");

            /*conf = new XMLConfig();
            conf.Choices = new List<Choice>();
            var c1 = new Choice();
            c1.Detect = "banana";
            c1.Response = "boat";
            conf.Choices.Add(c1);
            */
            //conf.SaveXml("XMLConfig.xml");
            InitializeComponent();
        }

        // Simple handler for the SpeechRecognized event.
        void sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            //MessageBox.Show(e.Result.Text);
            //sendOSCMessage(e.Result.Text);
            foreach (Choice c in conf.Choices)
            {
                if (String.Compare(c.Detect, e.Result.Text) == 0)
                {
                    textBox1.AppendText("\r\ndetected \"" + c.Detect + "\", sending response \"" + c.Response + "\"");
                    sendOSCMessage(c.Response);
                    return;
                }
            }
        }

        SpeechRecognitionEngine sre;

        private void Form1_Load_1(object sender, EventArgs e)
        {
            textBox1.AppendText("\r\nusing culture " + conf.CultureInfo);
            CultureInfo ci = new CultureInfo(conf.CultureInfo);

            sre = new SpeechRecognitionEngine(ci);

            Choices choices = new Choices();
            foreach(Choice c in conf.Choices)
            {
                choices.Add(c.Detect);
                textBox1.AppendText("\r\n" + c.Detect + " :: " + c.Response);
            }

            GrammarBuilder gb = new GrammarBuilder();
            gb.Culture = ci;
            gb.Append(choices);

            // Create the actual Grammar instance, and then load it into the speech recognizer.
            Grammar g = new Grammar(gb);
            sre.LoadGrammar(g);

            // Register a handler for the SpeechRecognized event.
            sre.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(sre_SpeechRecognized);
            sre.SetInputToDefaultAudioDevice();
            sre.RecognizeAsync(RecognizeMode.Multiple);

            textBox1.AppendText("\r\nsending " + conf.ReadyMessage + " to " + conf.IPAddress + ":" + conf.Port + "" + conf.MessageAddress);
            sendOSCMessage(conf.ReadyMessage);
            //MessageBox.Show(IPAddress.Loopback.ToString());

        }

        //public static readonly int Port = 7400;

        private void sendOSCMessage(string msg)
        {
            var message = new SharpOSC.OscMessage(conf.MessageAddress, msg);
            var sender = new SharpOSC.UDPSender(conf.IPAddress, conf.Port);
            sender.Send(message);
        }

    }
}
